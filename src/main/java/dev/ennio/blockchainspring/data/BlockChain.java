package dev.ennio.blockchainspring.data;

import dev.ennio.blockchainspring.common.MerkleHelper;
import dev.ennio.blockchainspring.common.Tuple;

import java.util.ArrayList;
import java.util.List;

public class BlockChain implements IBlockChain {
    private final List<Block> blocks;

    public BlockChain(int genesisIndex) {
        this.blocks = new ArrayList<>();

        if (genesisIndex == 0) {
            List<String> genesisData = List.of("Genesis block");
            Tuple<String, String> hashNonceMap = MerkleHelper
                    .findHash(0, "00000000000000",genesisData);

            this.blocks.add(new Block(0, "00000000000000", hashNonceMap.first, hashNonceMap.second, genesisData));
        }

        System.out.println("Blockchain initialized");
    }

    @Override
    public Block getBlock(int index) {
        return this.blocks.get(index);
    }

    @Override
    public int getBlockAmount() {
        return this.blocks.size();
    }

    @Override
    public int addBlock(int index, String previousHash, String hash, String nonce, List<String> merkle) {
        String blockHeader = index + previousHash + MerkleHelper.getMerkleRoot(merkle) + nonce;

        if (MerkleHelper.verifyBlock(blockHeader, hash) && index == this.blocks.size()) {
            this.blocks.add(new Block(index, hash, previousHash, nonce, merkle));
            return 1;
        }

        return 0;
    }

    @Override
    public String getBackBlockHash() {
        return this.blocks.get(this.blocks.size() - 1).getHash();
    }

    @Override
    public List<Block> getBlockChain() {
        return this.blocks;
    }

    @Override
    public void replaceChain(List<Block> newChain) {
        this.blocks.clear();
        this.blocks.addAll(newChain);
    }
}
