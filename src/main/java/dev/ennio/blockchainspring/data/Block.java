package dev.ennio.blockchainspring.data;

import java.util.List;

/**
 * Block class that holds all required data for a block.
 */
public class Block {
    private final int index;
    private final String hash;
    private final String previousHash;
    private final String nonce;
    private final long timestamp;
    private final List<String> data;

    public Block(
            int index,
            String hash,
            String previousHash,
            String nonce,
            List<String> data) {
        this.index = index;
        this.hash = hash;
        this.previousHash = previousHash;
        this.nonce = nonce;
        this.timestamp = System.currentTimeMillis();
        this.data = data;
    }

    public int getIndex() {
        return index;
    }

    public String getHash() {
        return hash;
    }

    public String getPreviousHash() {
        return previousHash;
    }

    public String getNonce() {
        return nonce;
    }

    public long getTimestamp() {
        return timestamp;
    }

    public List<String> getData() {
        return data;
    }
}