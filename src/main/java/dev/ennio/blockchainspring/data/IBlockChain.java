package dev.ennio.blockchainspring.data;

import java.util.List;

public interface IBlockChain {
    Block getBlock(int index);
    int getBlockAmount();
    int addBlock(int index, String previousHash, String hash, String nonce, List<String> merkle);
    String getBackBlockHash();
    List<Block> getBlockChain();
    void replaceChain(List<Block> blockChain);
}
