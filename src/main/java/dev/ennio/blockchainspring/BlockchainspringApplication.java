package dev.ennio.blockchainspring;

import dev.ennio.blockchainspring.data.BlockChain;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BlockchainspringApplication
{
    private static BlockChain blockChain;
    public static void main(String[] args)
    {
        blockChain = new BlockChain(0);
        SpringApplication.run(BlockchainspringApplication.class, args);
    }

    public static BlockChain getBlockChain()
    {
        return blockChain;
    }

}
