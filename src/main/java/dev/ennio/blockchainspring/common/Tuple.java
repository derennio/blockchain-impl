package dev.ennio.blockchainspring.common;

public class Tuple<Tx, Ty> {
    public Tx first;
    public Ty second;

    public Tuple(Tx first, Ty second) {
        this.first = first;
        this.second = second;
    }
}
