package dev.ennio.blockchainspring.common;

import java.util.ArrayList;
import java.util.List;

public class MerkleHelper {
    /**
     * Builds the merkle tree for the given list of transactions.
     * @param transactions The list of transactions.
     * @return The root of the merkle tree.
     */
    public static String getMerkleRoot(List<String> transactions) {
        List<String> hashes = new ArrayList<>();
        for (String transaction : transactions) {
            hashes.add(HashUtil.sha256(transaction));
        }
        return buildTree(hashes);
    }

    public static Tuple<String, String> findHash(int index, String previousHash, List<String> transactions) {
        String header = index + previousHash + getMerkleRoot(transactions);
        for(int nonce = 0; nonce < 100000; nonce++) {
            String blockHash = HashUtil.sha256(header + nonce);
            if(blockHash.startsWith("00")) {
                return new Tuple<>(blockHash, String.valueOf(nonce));
            }
        }

        return new Tuple<>("ERROR", "ERROR");
    }

    public static boolean verifyBlock(String blockHash, String header) {
        if (!blockHash.startsWith("00")) {
            return false;
        }

        return blockHash.equals(HashUtil.sha256(header));
    }

    /**
     * Builds the merkle tree for the given list of transactions.
     * @param children The list of transactions (containing hashed values).
     * @return The root of the merkle tree.
     */
    private static String buildTree(List<String> children) {
        ArrayList<String> parents = new ArrayList<>();

        while (children.size() != 1) {
            int index = 0, length = children.size();
            while (index < length) {
                String leftChild = children.get(index);
                String rightChild;

                if ((index + 1) < length) {
                    rightChild = children.get(index + 1);
                } else {
                    rightChild = leftChild;
                }

                String parentHash = HashUtil.sha256(leftChild + rightChild);
                parents.add(parentHash);
                index += 2;
            }
            children = parents;
            parents = new ArrayList<>();
        }
        return children.get(0);
    }
}
