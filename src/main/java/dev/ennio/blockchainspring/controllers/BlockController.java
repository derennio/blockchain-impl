package dev.ennio.blockchainspring.controllers;

import dev.ennio.blockchainspring.BlockchainspringApplication;
import dev.ennio.blockchainspring.data.Block;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("block")
public class BlockController {
    @GetMapping("/currentChain")
    public List<Block> getCurrentChain() {
        return BlockchainspringApplication.getBlockChain().getBlockChain();
    }

    @PostMapping("/newChain")
    public void replaceChain(List<Block> newChain) {
        BlockchainspringApplication.getBlockChain().replaceChain(newChain);
    }
}
