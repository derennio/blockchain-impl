package dev.ennio.blockchainspring.controllers;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author ennio
 **/
@RestController
public class TestController
{
    @GetMapping("/test")
    public String test()
    {
        return "Hello World";
    }
}
